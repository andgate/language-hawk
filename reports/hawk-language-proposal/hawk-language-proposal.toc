\contentsline {part}{I\hspace {1em}The Hawk Language}{5}
\contentsline {chapter}{\numberline {1}Introduction}{7}
\contentsline {section}{\numberline {1.1}A first section}{7}
\contentsline {section}{\numberline {1.2}Another section}{7}
\contentsline {chapter}{\numberline {2}Lexical Structure}{9}
\contentsline {chapter}{\numberline {3}Expressions}{11}
\contentsline {part}{II\hspace {1em}The Hawk Libraries}{13}
\contentsline {chapter}{\numberline {4}Data.Maybe}{15}
\contentsline {part}{III\hspace {1em}Hawk Language Implementation}{17}
\contentsline {chapter}{\numberline {5}Transformations}{19}
\contentsline {part}{IV\hspace {1em}Hawk Type System}{21}
\contentsline {chapter}{\numberline {6}Definitions}{25}
\contentsline {chapter}{\numberline {7}Proofs}{27}
\contentsfinish 
