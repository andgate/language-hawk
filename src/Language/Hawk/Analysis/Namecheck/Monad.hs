
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
module Language.Hawk.Analysis.Namecheck.Monad where

import Control.Monad.Writer hiding (Alt)
import Control.Monad.Reader
import Control.Monad.State.Lazy

import Language.Hawk.Analysis.Namecheck.Scope
import Language.Hawk.Analysis.Namecheck.Message

-- Helper types to manage reader/writer stack...
type MonadNameCheck m = (MonadState LocalScope m, MonadReader GlobalScope m, MonadWriter [NcMsg] m)
type Namecheck a = StateT LocalScope (ReaderT GlobalScope (Writer [NcMsg])) a

-- Helper function to run reader/writer stack...
runNamecheck :: Namecheck a -> GlobalScope -> (a, [NcMsg])
runNamecheck m env = runWriter $ runReaderT (evalStateT m mempty) env


