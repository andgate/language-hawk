module Language.Hawk.Analysis.Typecheck.Unify where

import Language.Hawk.Analysis.Typecheck.Monad
import Language.Hawk.Syntax.Concrete

unify :: MonoType -> MonoType -> Tc ()
unify _ _ = undefined