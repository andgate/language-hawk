{-# LANGUAGE  LambdaCase #-}
{-# LANGUAGE  RecordWildCards #-}
module Language.Hawk.Analysis.Typecheck.Substitution where

import Language.Hawk.Syntax.Concrete

import Prelude hiding (lookup)

import Data.HashMap.Strict (HashMap)
import Data.IntMap.Strict (IntMap)
import Data.Maybe
import Data.Text (Text)

import qualified Data.HashMap.Strict as HMap
import qualified Data.IntMap.Strict  as IMap 


data Subst
  = Subst
    { substFree   :: HashMap Text Type 
    , substSkolem :: IntMap Type
    }


emptySubst :: Subst
emptySubst = Subst mempty mempty

insert :: TVar -> Type -> Subst -> Subst
insert tv ty Subst{..}
  = case tv of
      TvBound _ n -> Subst (HMap.insert n ty substFree) substSkolem
      TvSkolem _ _ i -> Subst substFree (IMap.insert i ty substSkolem)

lookup :: TVar -> Subst -> Maybe Type
lookup tv Subst{..} = case tv of
  TvBound _ n -> HMap.lookup n substFree
  TvSkolem _ _ i -> IMap.lookup i substSkolem

fromList :: [(TVar, Type)] -> Subst
fromList = foldr (uncurry insert) emptySubst

subst :: [TVar] -> [Type] -> Type -> Type
subst tvs tys ts = substType env ts
  where
    env = fromList $ zip tvs tys

substType :: Subst -> Type -> Type
substType env = \case
  ty@(TVar n) -> fromMaybe ty (lookup n env)
